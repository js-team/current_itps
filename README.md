# Pseudo-project to track ITPs for packages that will be maintained within the JavaScript maintainers team.

**CAUTION**: this is a work in progress and is not yet the official workflow for the team.

Original announcement: https://alioth-lists.debian.net/pipermail/pkg-javascript-devel/2018-May/026131.html

Provisional workflow:

1. manually create issues for all your open ITPs

2. open the issue board:
   https://salsa.debian.org/js-team/current_itps/boards

3. move your ITPs in the right column.

The columns correspond to the "stage" each ITP is at:

- **Backlog**: by default new issues end up here
 
- **RFC**: Requesting comments from all members of the js-team; this is intended to be used when packaging work is almost complete and the maintainer would like to have the other team members to have a look at it (at this point you should add a link to the salsa repo to make this review easier)

- **RFS**: Request for sponsorship; moving an ITP into this column could replace the usual RFS:... mail to the mailing list

- **NEW**: Is stuck in the ftp-master NEW queue 

- **Done**: ACCEPTED packages

For how to use the gitlab kanban ("issue board") see:
https://docs.gitlab.com/ee/user/project/issue_board.html

# Caution

Do not add permanent, important updates to these issues: these belong to the BTS !

The salsa tracker should be used in a complementary way to the BTS to keep track
of volatile, transient information (that currently flows through the mailing
list, IRC, and the ftp-maser NEW queue).

# TODO

- automatically close the issues linked to ITPs that have been closed, see: https://lists.debian.org/debian-devel/2018/03/threads.html#00318

- automatically move issues to the NEW column, requiring no additional action from the sponsor, see: https://salsa.debian.org/paolog-guest/ftp_master_fancy_new

- automatize the creation of issues from ITPs (tricky !)